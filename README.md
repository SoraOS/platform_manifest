**Download "repo"**

	mkdir ~/bin
	PATH=~/bin:$PATH
	curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
	chmod a+x ~/bin/repo
	
**Create work folder**

	mkdir SoraOS
	cd SoraOS

**Configure Git**

    git config --global user.name "Your Name"
    git config --global user.email "you@example.com"
	

**Initialize the repo (Development branch)**

	repo init -u https://gitlab.com/SoraOS/platform_manifest.git -b 10

**Initialize the repo (Stable release)**

	repo init -u https://gitlab.com/SoraOS/platform_manifest.git -b refs/tags/QQ2A.200405.005
	
**Download the source**

	repo sync -j16